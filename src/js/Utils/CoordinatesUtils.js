import {DEFAULT_WIDTH, DEFAULT_HEIGHT, SINGLE_ELEMENT_SIZE} from '../canvas';
import {getScale as scale} from '../World/Camera';

function getElementSize() {
	return SINGLE_ELEMENT_SIZE * parseFloat(scale());
}

export function xWorldPositionToXIndex(worldPositionX) {
	const elementSize = getElementSize();
	worldPositionX = (worldPositionX < 0) ? worldPositionX - elementSize : worldPositionX;
	return parseInt(worldPositionX / elementSize);
}

export function yWorldPositionToYIndex(worldPositionY) {
	const elementSize = getElementSize();
	worldPositionY = (worldPositionY < 0) ? worldPositionY - elementSize : worldPositionY;
	return parseInt(worldPositionY / elementSize);
}

export function xIndexToXWorldPosition(xIndex) {
	const elementSize = getElementSize();
	return parseInt(xIndex * elementSize);
}

export function yIndexToYWorldPosition(yIndex) {
	const elementSize = getElementSize();
	return parseInt(yIndex * elementSize);
}