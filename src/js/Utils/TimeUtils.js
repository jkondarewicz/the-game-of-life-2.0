export default function time() {
	return new Date().getTime();
}