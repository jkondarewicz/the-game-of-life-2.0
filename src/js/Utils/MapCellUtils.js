
export function generateKeyByIndexes(xIndex, yIndex) {
	return xIndex + ';' + yIndex;
}

export function isCellSet(xIndex, yIndex, aliveCellsMap) {
	const key = generateKeyByIndexes(xIndex, yIndex);
	return isCellSetCheckByKey(key, aliveCellsMap);
}

export function isCellSetCheckByKey(key, aliveCellsMap) {
	if(aliveCellsMap.has(key) && aliveCellsMap.get(key) === true) {
		return true;
	}
	return false;
}

export function aliveCellIndexesFromKey(key) {
	const indexes = key.split(';');
	return {xIndex: parseInt(indexes[0]), yIndex: parseInt(indexes[1])};
}
