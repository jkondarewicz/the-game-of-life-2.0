import QuadRenderer from './Renderers/QuadRenderer';
import {mouseListener as ScreenMouseListener} from './World/ScreenMove';
import MasterRenderer from './Renderers/MasterRenderer';
import GameOfLife from './Logic/GameOfLife';
import time from './Utils/TimeUtils';

const MS_PER_UPDATE = 60;

let lastKnownScrollPosition = 0;
let ticking = false;

function callZoomLListener(e) {
	console.log(e);
		const gameOfLife = this.gameOfLife;
		console.log(window.scrollY, lastKnownScrollPosition);
		const zoomIn = e.deltaY < 0;
		lastKnownScrollPosition = window.scrollY;
		if(!ticking) {
			window.requestAnimationFrame(function() {
				if(zoomIn) {
					gameOfLife.zoomIn();
				} else {
					gameOfLife.zoomOut();
				}
				ticking = false;
			})
		}
		ticking = true;
	}

export default class Game {
	
	init() {
		this.masterRenderer = new MasterRenderer();
		this.gameOfLife = new GameOfLife();
		callZoomLListener = callZoomLListener.bind(this);
		window.addEventListener('mousemove', function(e) {ScreenMouseListener(e.clientX, e.clientY);});
		window.addEventListener('keyup', this.gameOfLife.processKeyboardKeyUp);
		window.addEventListener('mousedown', this.gameOfLife.processMouseClick);
		window.addEventListener('mouseup', this.gameOfLife.processMouseClick);
		window.addEventListener('wheel', callZoomLListener);
		
		this.currentTime = 0;
		this.previousTime = time();
		this.elapsed = 0;
		this.lag = 0;
		this.gameLoop = this.gameLoop.bind(this);
		this.gameLoop();
	}
	
	update() {
		this.gameOfLife.nextCycle();
		this.masterRenderer.setAliveCells(this.gameOfLife.getAliveCells());
		this.masterRenderer.setLiftingShape(this.gameOfLife.getLiftingShape());
	}
	
	render() {
		this.masterRenderer.render();
	}
	
	gameLoop() {
		window.requestAnimationFrame(this.gameLoop);

		this.currentTime = time();
		this.elapsed = this.currentTime - this.previousTime;
		this.previousTime = this.currentTime;
		this.lag += this.elapsed;
		
		this.gameOfLife.processInput();
		while(this.lag >= MS_PER_UPDATE) {
			this.update();
			this.lag -= MS_PER_UPDATE;
		}
		this.render();
	}
	
}
