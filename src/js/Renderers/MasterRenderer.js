import {getXWorldOffset as xOffset, getYWorldOffset as yOffset, getScale as scale} from '../World/Camera';
import {DEFAULT_WIDTH as screenWidth, DEFAULT_HEIGHT as screenHeight, SINGLE_ELEMENT_SIZE} from '../canvas';
import {xWorldPositionToXIndex, yWorldPositionToYIndex, xIndexToXWorldPosition, yIndexToYWorldPosition} from '../Utils/CoordinatesUtils';
import context from '../Utils/CanvasContext';
import QuadRenderer from './QuadRenderer';
import {getMouseXPosition, getMouseYPosition} from '../World/ScreenMove';
import {isCellSet} from '../Utils/MapCellUtils';
import {getState, STATES} from '../Logic/GameState';

const QUAD_HOVER_COLOR = "#ff0";
const QUAD_COLOR = "#f6f";
const QUAD_ALIVE = "#00f";

export default class MasterRenderer {
	
	constructor() {
		this.aliveCells = new Map();
		this.liftingShape = new Map();
	}
	
	setAliveCells(aliveCells) {
		this.aliveCells = aliveCells;
	}
	
	setLiftingShape(liftingShape) {
		this.liftingShape = liftingShape;
	}
	
	render() {
		context().clearRect(0, 0, screenWidth, screenHeight);
		context().setTransform(1, 0, 0, 1, 0, 0);
		// context().scale(scale(), scale());
		context().translate(-xOffset(), -yOffset());
		this.renderQuads();
	}
	
	renderQuads() {
		var quads = [];
		const mouseX = getMouseXPosition();
		const mouseY = getMouseYPosition();
		const quadSelectedXIndex = xWorldPositionToXIndex(mouseX);
		const quadSelectedYIndex = yWorldPositionToYIndex(mouseY);
		
		const elementSize = SINGLE_ELEMENT_SIZE * parseFloat(scale()); 
		for(var x = xOffset() - elementSize; x < screenWidth + xOffset() + elementSize; x += elementSize) {
			for(var y = yOffset() - elementSize; y < screenHeight + yOffset() + elementSize; y += elementSize) {
				const xIndex = xWorldPositionToXIndex(x);
				const yIndex = yWorldPositionToYIndex(y);
				var color = QUAD_COLOR;
				if(getState() === STATES.PAUSE && quadSelectedXIndex === xIndex && quadSelectedYIndex === yIndex && this.liftingShape.size === 0) {
					color = QUAD_HOVER_COLOR;
				}
				if(getState() === STATES.PAUSE && isCellSet(xIndex, yIndex, this.liftingShape)) {
					color = QUAD_HOVER_COLOR;
				}
				if(isCellSet(xIndex, yIndex, this.aliveCells)) {
					color = QUAD_ALIVE;
				}
				const quad = {
					x: xIndexToXWorldPosition(xIndex),
					y: yIndexToYWorldPosition(yIndex),
					sizeX: elementSize,
					sizeY: elementSize,
					color: color
				};
				QuadRenderer.render(quad);
			}
		}
		return quads;
	}
	
}
