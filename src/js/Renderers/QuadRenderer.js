import context from '../Utils/CanvasContext';

const QUAD_BORDER_COLOR = "white";
const QUAD_BORDER_WEIGHT = "0.5";

class QuadRenderer {
	
	renderAll(quads) {
		const that = this;
		quads.forEach(quad => that.render(quad));
	}
	
	render(quad) {
		context().fillStyle = quad.color;
		context().fillRect(quad.x, quad.y, quad.sizeX, quad.sizeY);
		
		context().strokeStyle = QUAD_BORDER_COLOR;
		context().lineWidth = QUAD_BORDER_WEIGHT;
		context().strokeRect(quad.x, quad.y, quad.sizeX, quad.sizeY);
	}
	
}

const quadRendererInstance = new QuadRenderer();
export default quadRendererInstance;

