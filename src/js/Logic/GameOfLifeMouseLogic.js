import {setState, getState, STATES} from './GameState';
import {xWorldPositionToXIndex, yWorldPositionToYIndex} from '../Utils/CoordinatesUtils';
import {generateKeyByIndexes, isCellSet, isCellSetCheckByKey, aliveCellIndexesFromKey} from '../Utils/MapCellUtils';
import {isUpMove, isDownMove, isLeftMove, isRightMove, getMouseXPosition, getMouseYPosition} from '../World/ScreenMove';
import time from '../Utils/TimeUtils';

function shouldSelectCell(previouslySelectedCell, cellX, cellY) {
	if(previouslySelectedCell === null) return true;
	return !(previouslySelectedCell.xIndex === cellX && previouslySelectedCell.yIndex === cellY);
}

class MouseLogic {
	
	constructor() {
		this.liftingShape = false;
		this.holdingMouseClick = false;
		this.previouslySelectedCell = null;
		this.lastClickSetShape = false;
	}
	
	setHoldingMouseClick(holdingMouseClick) {
		if(this.holdingMouseClick !== holdingMouseClick) {
			this.previouslySelectedCell = null;
			if(!holdingMouseClick) {
				this.lastClickSetShape = false;
			}
		}
		this.holdingMouseClick = holdingMouseClick;
	}
	
	generateLiftingShapeHover() {
		const liftingShapeCells = new Map();
		if(this.liftingShape !== false) {
			const x = getMouseXPosition();
			const y = getMouseYPosition();
			const xIndex = xWorldPositionToXIndex(x);
			const yIndex = yWorldPositionToYIndex(y);
			const heightOfShape = this.liftingShape.shape.length;
			for(var i = 0; i < heightOfShape; i++) {
				const widthOfShape = this.liftingShape.shape[i].length;
				for(var j = 0; j < widthOfShape; j++) {
					if(this.liftingShape.shape[i][j] === true) {
						liftingShapeCells.set(generateKeyByIndexes(xIndex + j - parseInt(widthOfShape / 2), yIndex + i - parseInt(heightOfShape / 2)), true);
					}
				}
			}
		}
		return liftingShapeCells;
	}
	
	processMouseToGenerateLiftingShapeCells(liftingShape, liftingShapeCells, aliveCells) {
		this.liftingShape = liftingShape;
		if(this.holdingMouseClick && getState() === STATES.PAUSE) {
			const x = getMouseXPosition();
			const y = getMouseYPosition();
			const xIndex = xWorldPositionToXIndex(x);
			const yIndex = yWorldPositionToYIndex(y);
			
			if(this.liftingShape !== false) {
				this.liftingShape = false;
				var keys = liftingShapeCells.keys();
				this.lastClickSetShape = true;
				for(let key of keys) {
					aliveCells.set(key, true);
				}
				return false;
			}
			
		}
		return liftingShape;
	}
	
	processMouseToGenerateAliveCells(aliveCells) {
		if(this.holdingMouseClick && getState() === STATES.PAUSE && !this.lastClickSetShape) {
			const x = getMouseXPosition();
			const y = getMouseYPosition();
			const xIndex = xWorldPositionToXIndex(x);
			const yIndex = yWorldPositionToYIndex(y);
			if(this.liftingShape === false && shouldSelectCell(this.previouslySelectedCell, xIndex, yIndex)) {
				if(isCellSet(xIndex, yIndex, aliveCells)) {
					aliveCells.delete(generateKeyByIndexes(xIndex, yIndex));
				} else {
					aliveCells.set(generateKeyByIndexes(xIndex, yIndex), true);
				}
				this.previouslySelectedCell = {xIndex, yIndex};
			}
		}
	}
	
}

const mouseLogic = new MouseLogic();
export default mouseLogic;