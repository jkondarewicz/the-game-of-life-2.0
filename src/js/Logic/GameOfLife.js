import {isUpMove, isDownMove, isLeftMove, isRightMove, getMouseXPosition, getMouseYPosition} from '../World/ScreenMove';
import {getYWorldOffset, getXWorldOffset, moveXWorldOffset, moveYWorldOffset, zoomIn as cameraZoomIn, zoomOut as cameraZoomOut} from '../World/Camera';
import {setState, getState, STATES} from './GameState';
import {xWorldPositionToXIndex, yWorldPositionToYIndex} from '../Utils/CoordinatesUtils';
import {GUN, SHAPES} from './Shapes';
import {generateKeyByIndexes, isCellSet, isCellSetCheckByKey, aliveCellIndexesFromKey} from '../Utils/MapCellUtils';
import MouseLogic from './GameOfLifeMouseLogic';

const SPACE_KEY_CODE = 32;
const NUMBER_1_EVENT_CODE = 49;
const NUMBER_2_EVENT_CODE = 50;
const NUMBER_3_EVENT_CODE = 51;
const ESCAPE_KEY_CODE = 27;
const C_KEY_CODE = 67;
const R_KEY_CODE = 82;

function setAlivnessOfCellsInNextCycle(xIndex, yIndex, aliveCellsNow, checkedCells, aliveCellsInNextCycle) {
	const startXIndex = xIndex - 1;
	const endXIndex = xIndex + 1;
	const startYIndex = yIndex - 1;
	const endYIndex = yIndex + 1;
	
	const currentCellIsAlive = isCellSet(xIndex, yIndex, aliveCellsNow);
	var aliveNeighbours = (currentCellIsAlive) ? -1 : 0;
	checkedCells.set(generateKeyByIndexes(xIndex, yIndex), true);
	for(var x = startXIndex; x <= endXIndex; x++) {
		for(var y = startYIndex; y <= endYIndex; y++) {
			const neighborAlive = isCellSet(x, y, aliveCellsNow);
			if(currentCellIsAlive && !neighborAlive && !checkedCells.has(generateKeyByIndexes(x, y))) {
				setAlivnessOfCellsInNextCycle(x, y, aliveCellsNow, checkedCells, aliveCellsInNextCycle);
			}
			if(neighborAlive) {
				++aliveNeighbours;
			}
		}
	}
	if(currentCellIsAlive && aliveNeighbours >= 2 && aliveNeighbours <= 3) {
		aliveCellsInNextCycle.set(generateKeyByIndexes(xIndex, yIndex), true);
	} else if(!currentCellIsAlive && aliveNeighbours === 3) {
		aliveCellsInNextCycle.set(generateKeyByIndexes(xIndex, yIndex), true);
	}	
}

export default class GameOfLife {
		
	constructor() {
		this.processMouseClick = this.processMouseClick.bind(this);
		this.processKeyboardKeyUp = this.processKeyboardKeyUp.bind(this);
		this.aliveCells = new Map();
		this.liftingShapeCells = new Map();
		this.liftingShape = false;
		this.holdingMouseClick = false;
	}
		
	processInput() {
		const speed = 4;
		if(isUpMove()) moveYWorldOffset(speed);
		else if(isDownMove()) moveYWorldOffset(-speed);
		if(isRightMove()) moveXWorldOffset(speed);
		else if(isLeftMove()) moveXWorldOffset(-speed);
		
		this.liftingShapeCells = MouseLogic.generateLiftingShapeHover();
		MouseLogic.setHoldingMouseClick(this.holdingMouseClick);
		this.liftingShape = MouseLogic.processMouseToGenerateLiftingShapeCells(this.liftingShape, this.liftingShapeCells, this.aliveCells);
		if(this.liftingShape === false) this.liftingShapeCells = new Map();
		MouseLogic.processMouseToGenerateAliveCells(this.aliveCells);
	}
	
	processKeyboardKeyUp(keyEvent) {
		const keyCode = keyEvent.keyCode;
		switch(keyCode) {
			case SPACE_KEY_CODE:
				if(getState() === STATES.PAUSE) setState(STATES.SIMULATION);
				else if(getState() === STATES.SIMULATION) {
					this.liftingShape = false;
					this.liftingShapeCells = new Map();
					setState(STATES.PAUSE);
				}
			break;
			case NUMBER_1_EVENT_CODE:
				this.liftingShape = SHAPES.GUN;
			break;
			case NUMBER_2_EVENT_CODE:
				this.liftingShape = SHAPES.DAKOTA;
			break;
			case NUMBER_3_EVENT_CODE:
				this.liftingShape = SHAPES.GLIDER;
			break;
			case ESCAPE_KEY_CODE:
				this.liftingShape = false;
			break;
			case C_KEY_CODE:
				this.liftingShape = false;
				this.aliveCells = new Map();
				this.liftingShapeCells = new Map();
			break;
			case R_KEY_CODE:
				if(this.liftingShape !== false) this.liftingShape.rotate();
			break;
			
		}
	}
		
	zoomIn() {
		cameraZoomIn();
	}
	
	zoomOut() {
		cameraZoomOut();
	}
		
	processMouseClick(e) {
		if(e.type === "mousedown") this.holdingMouseClick = true;
		else if(e.type === "mouseup") this.holdingMouseClick = false;
	}
		
	nextCycle() {
		if(getState() === STATES.SIMULATION) {
			const checkedCells = new Map();
			const aliveCells = this.aliveCells.keys();
			const aliveCellsInNextCycle = new Map();
			for(const aliveCell of aliveCells) {
				const indexes = aliveCellIndexesFromKey(aliveCell);
				const xIndex = indexes.xIndex;
				const yIndex = indexes.yIndex;
				setAlivnessOfCellsInNextCycle(xIndex, yIndex, this.aliveCells, checkedCells, aliveCellsInNextCycle);
			}
			this.aliveCells = aliveCellsInNextCycle;
		}
	}
	
	getAliveCells() {
		return this.aliveCells;
	}
	
	getLiftingShape() {
		return this.liftingShapeCells;
	}
	
}