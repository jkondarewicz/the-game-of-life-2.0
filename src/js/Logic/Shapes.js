const GUN = [
	generateArray([24], 36),
	generateArray([22, 24], 36),
	generateArray([12, 13, 20, 21, 34, 35], 36),
	generateArray([11, 15, 20, 21, 34, 35], 36),
	generateArray([0, 1, 10, 16, 20, 21], 36),
	generateArray([0, 1, 10, 14, 16, 17, 22, 24], 36),
	generateArray([10, 16, 24], 36),
	generateArray([11, 15], 36),
	generateArray([12, 13], 36),
];

const GLIDER = [
	generateArray([0, 1, 2], 3),
	generateArray([0], 3),
	generateArray([1], 3),
];

const DAKOTA = [
	generateArray([1, 5], 5),
	generateArray([0], 5),
	generateArray([0, 5], 5),
	generateArray([0, 1, 2, 3, 4], 5)
];

function rotate(shape, rotation) {
	console.log(shape[0]);
	var newShape = [];
	if(rotation % 2 === 1) {
		console.log("R1");
		for(var x = 0; x < shape[0].length; x++) {
			var shapeX = [];
			for(var y = 0; y < shape.length; y++) {
				shapeX.push(shape[y][x]);
			}
			newShape.push(shapeX);
		}
	} else {
		console.log("R2");
		for(var x = shape[0].length - 1; x >= 0; x--) {
			var shapeX = [];
			for(var y = shape.length - 1; y >= 0; y--) {
				shapeX.push(shape[y][x]);
			}
			newShape.push(shapeX);
		}
	}
	return newShape;
}

function genShape(shape) {
	return {
		shape,
		rotation: 0,
		rotate: function() {
			this.rotation = (++this.rotation) % 4;
			this.shape = rotate(this.shape, this.rotation);
		}
	}
}

export const SHAPES = {GUN: genShape(GUN), GLIDER: genShape(GLIDER), DAKOTA: genShape(DAKOTA)};

function generateArray(indexes, elements) {
	const arr = [];
	var index = 0;
	for(var i = 0; i < elements; i++) {
		arr[i] = false;
		if(indexes[index] === i) {
			arr[i] = true;
			index++;
		}
	}
	return arr;
}