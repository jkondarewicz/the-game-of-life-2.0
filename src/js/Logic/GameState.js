export const STATES = {SIMULATION: 1, PAUSE: 2}

const possibleStates = Object.values(STATES);

var state = STATES.PAUSE;

function properlyState(state) {
	for(var i = 0; i < possibleStates.length; i++) {
		if(state === possibleStates[i]) return true;
	}
	return false;
}

export function setState(newState) {
	if(properlyState(newState)) state = newState;
}

export function getState() {
	return state;
}