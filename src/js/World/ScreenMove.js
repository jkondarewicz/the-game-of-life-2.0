import {DEFAULT_WIDTH, DEFAULT_HEIGHT} from '../canvas';
import {getXWorldOffset, getYWorldOffset} from './Camera';

const MOVE_SCREEN_PRECISION = 0.1;

const mouseXPositionToStartMoveScreenRight = 0.8 * DEFAULT_WIDTH;
const mouseYPositionToStartMoveScreenDown = 0.8 * DEFAULT_HEIGHT;
const mouseXPositionToStartMoveScreenLeft = 0.2 * DEFAULT_WIDTH;
const mouseYPositionToStartMoveScreenUp = 0.2 * DEFAULT_HEIGHT;

const VERTICAL_DIRECTIONS = {UP: 1, DOWN: -1};
const HORIZONTAL_DIRECTIONS = {LEFT: -1, RIGHT: 1};

let verticalDirection = null;
let horizontalDirection = null;

let xPosition = 0;
let yPosition = 0;

export function mouseListener(mouseXPosition, mouseYPosition) {
	xPosition = mouseXPosition;
	yPosition = mouseYPosition;
	
	if(mouseXPosition > mouseXPositionToStartMoveScreenRight) {
		horizontalDirection = HORIZONTAL_DIRECTIONS.RIGHT;
	} else if (mouseXPosition < mouseXPositionToStartMoveScreenLeft) {
		horizontalDirection = HORIZONTAL_DIRECTIONS.LEFT;
	} else {
		horizontalDirection = null;
	}
	if(mouseYPosition > mouseYPositionToStartMoveScreenDown) {
		verticalDirection = VERTICAL_DIRECTIONS.UP;
	} else if (mouseYPosition < mouseYPositionToStartMoveScreenUp) {
		verticalDirection = VERTICAL_DIRECTIONS.DOWN;
	} else {
		verticalDirection = null;
	}
} 

export function getMouseXPosition() {
	return xPosition + getXWorldOffset();
}

export function getMouseYPosition() {
	return yPosition + getYWorldOffset();
}

export function isUpMove() {
	return verticalDirection === VERTICAL_DIRECTIONS.UP;
}

export function isDownMove() {
	return verticalDirection === VERTICAL_DIRECTIONS.DOWN;
}

export function isRightMove() {
	return horizontalDirection === HORIZONTAL_DIRECTIONS.RIGHT;
}

export function isLeftMove() {
	return horizontalDirection === HORIZONTAL_DIRECTIONS.LEFT;
}