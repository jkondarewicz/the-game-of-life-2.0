import {SINGLE_ELEMENT_SIZE} from '../canvas';

let xWorldOffset = 0;
let yWorldOffset = 0;
let scale = 1;

const lowestZoom = 0.2;
const heighestZoom = 1.5;
const zoomSpeed = 0.05;

export function getXWorldOffset() {
	return xWorldOffset;
}

export function getYWorldOffset() {
	return yWorldOffset;
}

export function moveXWorldOffset(offset) {
	xWorldOffset += offset;
}

export function moveYWorldOffset(offset) {
	yWorldOffset += offset;
}

export function getScale() {
	return scale;
}

export function zoomOut() {
	var newScale = scale - zoomSpeed;
	if(newScale > lowestZoom) {
		scale = newScale;
		xWorldOffset -= SINGLE_ELEMENT_SIZE * scale;
		yWorldOffset -= SINGLE_ELEMENT_SIZE * scale;
	}
}

export function zoomIn() {
	var newScale = scale + zoomSpeed;
	if(newScale < heighestZoom) {
		scale = newScale;
		xWorldOffset += SINGLE_ELEMENT_SIZE * scale;
		yWorldOffset += SINGLE_ELEMENT_SIZE * scale;
	}
}

