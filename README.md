# The game of life 2.0

# Running app

To run application follow steps

1. `git clone https://gitlab.com/jkondarewicz/the-game-of-life-2.0.git`
2. Open directory `{path_to_root_of_application}/dist`
3. Open index.html

# Application input

Application has two states:

1. Simulation
2. Pause

Description of states:

1. At pause state you are allowed to setting aliveness of cells
2. At simulation state you are watching cycle of each cell

1. At pause state you can use input from keyboard:
    * 1 - gun structure
    * 2 - glider structure
    * 3 - dakota structure
    * ESC - if you are holding structure, you return to setting each cell individually
    * r - if you are holding structure, you rotate that structure
    * SPACE - change to simulation state
    * c - clear board (set all cells to dead)
    * MOUSE_CLICK - to change aliveness of current cell (dead -> alive, alive -> dead)
2. At simulation state tou can use input from keyboard
    * c - clear board (set all cells to dead)
    * SPACE - change to PAUSE state